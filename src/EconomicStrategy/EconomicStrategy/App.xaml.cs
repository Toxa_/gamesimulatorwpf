﻿using System;
using System.IO;
using System.Windows;
using AutoMapper;
using BusinessLogic.Config;
using BusinessLogic.Infrastructure.Mapper;
using BusinessLogic.Service;
using EconomicStrategy.Helpers;
using EconomicStrategy.Windows;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EconomicStrategy
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration Configuration { get; private set; }

        private void ConfigurationServiceAsync(IServiceCollection services)
        {
            ConfigurationManagerBll.Configuration(services,
                InternetHelper.CheckForInternetConnection()
                    ? Configuration.GetConnectionString("SqlConnectionRemote")
                    : Configuration.GetConnectionString("SqlConnection"));

            services.AddTransient(typeof(MainWindow));
            services.AddTransient(typeof(SaveWindow));

            services.AddSingleton(typeof(ProductionService));
            services.AddSingleton(typeof(HouseService));
            services.AddSingleton(typeof(WarehouseService));
            services.AddSingleton(typeof(SaveService));

            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MapperConfig()); });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true);
            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigurationServiceAsync(serviceCollection);
            ServiceProvider = serviceCollection.BuildServiceProvider();

            ServiceProvider.GetRequiredService<MainWindow>().Show();
        }
    }
}