﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BusinessLogic.Models_DTO;
using BusinessLogic.Service;
using Point = System.Drawing.Point;

namespace EconomicStrategy.Windows
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly HouseService _houseService;
        private readonly ProductionService _productBuildService;
        private readonly SaveService _saveService;

        private readonly WarehouseService _warehouseService;

        private double _coinUser = 5000;
        private ProgressBar _progressBar;
        private int _tagChoiceBuilding;

        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(SaveService saveService, ProductionService productionService,
            WarehouseService warehouseService, HouseService houseService)
        {
            WindowState = WindowState.Maximized;
            _warehouseService = warehouseService;
            _saveService = saveService;
            _houseService = houseService;

            _productBuildService = productionService;

            InitializeComponent();
            coinTextBlock.Text = _coinUser.ToString();

            _productBuildService.CreateProductEvent += CreateProduct;
            _houseService.PlusProfitEvent += PlusProfit;
            _productBuildService.PlusProgressEvent += PlusProgress;
            _productBuildService.BuildingsRentalEvent += BuildingRental;
            _saveService.LoadSaveEvent += LoadSave;
            var im = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"..\..\..\Image\Backgraund\Backgraung-Warehouse.jpg",
                    UriKind.Relative)),
                Stretch = Stretch.UniformToFill
            };
            LeftOptionBorder.Background = im;
            imgageCash.Source =
                new BitmapImage(new Uri(@"pack://application:,,,/" + @"Image\Icon\Elem of Control\cash.png"));
        }


        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            await _warehouseService.InitialWarehouseAsync().ConfigureAwait(false);
            await _houseService.InitialComponents().ConfigureAwait(false);
            await _productBuildService.InitialCompontntsAsync().ConfigureAwait(false);
        }

        private void LoadSave() // Загрузка сохранения
        {
            _coinUser = _saveService.Cash;
            coinTextBlock.Text = _coinUser.ToString();


            foreach (var item in _productBuildService.Buildings)
            {
                var border = new Border
                {
                    Tag = _productBuildService.ProductionBuildings.FirstOrDefault(x => x.Name == item.Name)
                };
                var image = new Image { Source = new BitmapImage(new Uri(@"pack://application:,,,/" + item.IconPath)) };
                image.MouseLeftButtonDown += BuildShowInfoClick;
                image.Tag = item.Id;
                border.Child = image;
                buildingGrid.Children.Add(border);
                Grid.SetColumn(border, item.Point.Y);
                Grid.SetRow(border, item.Point.X);
            }

            foreach (var item in _houseService.Houses)
            {
                var border = new Border
                {
                    Tag = _productBuildService.ProductionBuildings.FirstOrDefault(x => x.Name == item.Name)
                };
                var image = new Image
                { Source = new BitmapImage(new Uri(@"pack://application:,,,/" + item.HouseIconPath)) };
                image.MouseLeftButtonDown += HouseShowInfoClick;
                image.Tag = item.Id;
                border.Child = image;
                buildingGrid.Children.Add(border);
                Grid.SetColumn(border, item.Point.Y);
                Grid.SetRow(border, item.Point.X);
            }
        }

        public async Task PlusProfit(int profit) // Прибавления денег в бюджет игрока
        {
            await Task.Run(() =>
            {
                _coinUser += profit;
                Dispatcher?.Invoke(() => coinTextBlock.Text = _coinUser.ToString());
            });
        }

        private void BuildingGridLoaded(object sender, RoutedEventArgs e) // Стартовая инициализация сетки главного грида(Grid)
        {
            if (sender == null) return;

            for (var i = 0; i < ((Grid)sender).RowDefinitions.Count; i++)
                for (var j = 0; j < ((Grid)sender).ColumnDefinitions.Count; j++)
                {
                    var border = new Border();
                    var img = new Image();
                    img.MouseLeftButtonDown += BuildShowInfoClick;
                    border.MouseLeftButtonDown += (sender1, e1) => BuildingGridMouseDownAsync(sender1, e1);
                    border.Background = Brushes.Transparent;
                    border.Child = img;
                    buildingGrid.Children.Add(border);
                    Grid.SetColumn(border, j);
                    Grid.SetRow(border, i);
                }
        }

        private void BuildShowInfoClick(object sender, RoutedEventArgs e) // Событие клика на размещённое здание
        {
            if (optionsBuildBorder.Visibility == Visibility.Hidden) optionsBuildBorder.Visibility = Visibility.Visible;

            if (optionsBuildGrid.Tag != null)
                _productBuildService.UnsubscribeEventPlusProgress((int)optionsBuildGrid.Tag);

            optionsBuildGrid.Children.Clear();
            if (!int.TryParse((sender as Image)?.Tag.ToString(), out var id)) return;

            optionsBuildGrid.Tag = id;
            _productBuildService.SubscribeEventPlusProgress(id);
            var build = _productBuildService.Buildings.First(x => x.Id == id);

            var img = new Image { Source = (sender as Image)?.Source };
            optionsBuildGrid.Children.Add(img);

            if (build.FirstProducts.Count != 0)
            {
                var imageFirstProduct = new Image
                {
                    Source = new BitmapImage(new Uri(@"pack://application:,,,/" + build.FirstProducts[0].IconPath)),
                    Height = 60,
                    Width = 60,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(10, 0, 0, 0)
                };
                var countProductText = new TextBlock { Text = "   " + build.Cells[0].CountProduct.ToString() };
                var stackPanel = new StackPanel { HorizontalAlignment = HorizontalAlignment.Left };
                stackPanel.Children.Add(imageFirstProduct);
                stackPanel.Children.Add(countProductText);

                optionsBuildGrid.Children.Add(stackPanel);
                Grid.SetRow(stackPanel, 3);
            }

            // var progressBar = new ProgressBar() {Name = "BuildProgress", Background = Brushes.DarkBlue };
            _progressBar = new ProgressBar { Name = "BuildProgress", Background = Brushes.DarkBlue };
            var textBlockName = new TextBlock { Text = build.Name, FontSize = 18 };
            var textBlockRental = new TextBlock { Text = "Содержание: " + build.Rental.ToString(), FontSize = 18, 
                Margin = new Thickness(0,30,0,0), HorizontalAlignment = HorizontalAlignment.Right };
            var textBlockPower = new TextBlock
            { Text = "=>  " + build.Power + "% ", FontSize = FontSize = 20, Margin = new Thickness(80, 0, 0, 0) };

            optionsBuildGrid.Children.Add(_progressBar);

            optionsBuildGrid.Children.Add(textBlockName);
            optionsBuildGrid.Children.Add(textBlockRental);
            optionsBuildGrid.Children.Add(textBlockPower);

            Grid.SetRow(img, 0);

            Grid.SetRow(_progressBar, 1);
            Grid.SetRow(textBlockName, 2);
            Grid.SetRow(textBlockRental, 3);
            Grid.SetRow(textBlockPower, 3);
        }

        private void InitialWarehouse() // Инициализация склада
        {
            var image = new ImageBrush
            {
                ImageSource =
                    new BitmapImage(new Uri(@"pack://application:,,,/" + @"Image\Backgraund\Backgraung-Warehouse.jpg")),
                Stretch = Stretch.UniformToFill
            };
            LeftOptionBorder.Background = image;

            if (LeftOptionGrid.Children.Count != 0) LeftOptionGrid.Children.Clear();

            var i = 0;
            var j = 0;
            foreach (var item in _warehouseService.Warehouse.CellInWarehouses)
            {
                var textBlock = new TextBlock
                {
                    Text = item.CountProduct.ToString(),
                    Margin = new Thickness(10, 0, 15, 0),
                    FontSize = 18,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.DarkBlue,
                    Background = Brushes.WhiteSmoke,
                    VerticalAlignment = VerticalAlignment.Top
                };
                var img = new Image
                {
                    Source = new BitmapImage(new Uri(@"pack://application:,,,/" + item.Product.IconPath)),
                    Tag = item.Product.Id,
                    Width = 80,
                    Height = 80,
                    Stretch = Stretch.UniformToFill,
                };
                img.MouseDown += ProductClick;
                var stackPanel = new StackPanel
                {
                    //  Margin = new Thickness(10, 10, 0, 0),
                    Orientation = Orientation.Vertical
                };
                stackPanel.Children.Add(textBlock);
                stackPanel.Children.Add(img);
                LeftOptionGrid.Children.Add(stackPanel);
                if (i >= LeftOptionGrid.ColumnDefinitions.Count)
                {
                    i = 0;
                    j++;
                }

                Grid.SetColumn(textBlock, i);
                Grid.SetColumn(stackPanel, i++);
                Grid.SetRow(textBlock, j);
                Grid.SetRow(stackPanel, j);
            }
        }

        private void WarehouseButtonClick(object sender, RoutedEventArgs e)
        {
            InitialWarehouse();
        }

        private void HouseButtonClick(object sender, RoutedEventArgs e) //Показ всех доступных для постройки зданий
        {
            houseOptionBorder.Visibility = houseOptionBorder.Visibility == Visibility.Visible
                ? Visibility.Hidden
                : Visibility.Visible;

            var startPath = @"..\..\..\";
            int i = 0, j = 0;
            foreach (var item in _productBuildService.ProductionBuildings)
            {
                var img = new Image
                {
                    Source = new BitmapImage(new Uri(startPath + item.IconPath, UriKind.Relative)),
                    Tag = item.Id
                };
                var stackPanel = new StackPanel { Margin = new Thickness(5, 5, 0, 0) };
                stackPanel.Children.Add(img);
                img.MouseRightButtonDown += ProductionBuildingInfoClick;
                img.MouseLeftButtonDown += ProductionBuildingClick;
                buildingOptionsGrid.Children.Add(stackPanel);

                if (i >= buildingOptionsGrid.ColumnDefinitions.Count)
                {
                    ++j;
                    i = 0;
                }

                Grid.SetColumn(stackPanel, i++);
                Grid.SetRow(stackPanel, j);
            }

            foreach (var item in _houseService.HouseTypes)
            {
                var img = new Image
                {
                    Source = new BitmapImage(new Uri(startPath + item.HouseIconPath, UriKind.Relative)),
                    Tag = item.Id
                };
                var stackPanel = new StackPanel { Margin = new Thickness(5, 5, 0, 0) };
                stackPanel.Children.Add(img);
                img.MouseLeftButtonDown += HouseClick;
                buildingOptionsGrid.Children.Add(stackPanel);

                if (i >= buildingOptionsGrid.ColumnDefinitions.Count)
                {
                    ++j;
                    i = 0;
                }

                Grid.SetColumn(stackPanel, i++);
                Grid.SetRow(stackPanel, j);
            }
        }

        private void HouseShowInfoClick(object sender, RoutedEventArgs e) //Событие клика на жилое здание
        {
            if (optionsBuildBorder.Visibility == Visibility.Hidden) optionsBuildBorder.Visibility = Visibility.Visible;

            optionsBuildGrid.Children.Clear();
            if (!int.TryParse((sender as Image)?.Tag.ToString(), out var id)) return;

            var house = _houseService.Houses.First(x => x.Id == id);

            var img = new Image { Source = (sender as Image)?.Source };
            optionsBuildGrid.Children.Add(img);
            Grid.SetRow(img, 0);


            var textStackPanel = new StackPanel { Orientation = Orientation.Horizontal };
            var textBlockName = new TextBlock { Text = house.Name, FontSize = 20 };
            var textBlockCountCitizens = new TextBlock
            {
                Text = "    Жителей:" + house.CurrentlyCountCitizens,
                FontSize = 20,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            var textBlockProfit = new TextBlock
            { Text = "Доход:  " + house.Profit, FontSize = 20, HorizontalAlignment = HorizontalAlignment.Center };
            textStackPanel.Children.Add(textBlockName);
            textStackPanel.Children.Add(textBlockCountCitizens);
            Grid.SetRow(textStackPanel, 1);

            optionsBuildGrid.Children.Add(textBlockProfit);
            Grid.SetRow(textBlockProfit, 2);

            var imageStackPanel = new StackPanel { Orientation = Orientation.Horizontal };
            optionsBuildGrid.Children.Add(imageStackPanel);
            foreach (var border in house.NeedProducts.Select(item => new Border
            {
                BorderThickness = new Thickness(10),
                BorderBrush =
                    house.Cells.FirstOrDefault(x => x.Product != null && x.Product.Id == item.Id).CountProduct == 0
                        ? Brushes.Gray
                        : Brushes.LightGreen,
                Child = new Image
                {
                    Source = new BitmapImage(new Uri("pack://application:,,,/" + item.IconPath)),
                    Margin = new Thickness(10, 0, 0, 0)
                }
            }))
                imageStackPanel.Children.Add(border);
            Grid.SetRow(imageStackPanel, 3);
            optionsBuildGrid.Children.Add(textStackPanel);
        }

        private void ProductionBuildingClick(object sender, RoutedEventArgs e) //выбор здания(ProductionBuilding) для постройки
        {
            if (!int.TryParse(((Image)sender).Tag.ToString(), out var tag)) return;

            _tagChoiceBuilding = tag;
            var build = _productBuildService.ProductionBuildings.First(x => x.Id == _tagChoiceBuilding);
            choiceElemImage.Source = new BitmapImage(new Uri("pack://application:,,,/" + build.IconPath));
        }

        private void HouseClick(object sender, RoutedEventArgs e) //выбор жилого дома(House) для постройки
        {
            if (!int.TryParse(((Image)sender).Tag.ToString(), out var tag)) return;

            var house = _houseService.HouseTypes.First(x => x.Id == tag);
            _tagChoiceBuilding = tag + 100;
            choiceElemImage.Source = new BitmapImage(new Uri("pack://application:,,,/" + house.HouseIconPath));
        }

        private void
            ProductionBuildingInfoClick(object sender,
                RoutedEventArgs e) // Вывод иформации о здании(ProductionBuilding)
        {
            foreach (var item in _productBuildService.ProductionBuildings)
            {
                if (((Image)sender).Tag.ToString() != item.Id.ToString()) continue;

                MessageBox.Show($"{item.Name}\nПроизводит: {item.FinalProduct.Name},\nВремя производства: {item.CoolDawn},\nСодержание: {item.Rental}");
                break;
            }
        }

        private void ProductClick(object sender, RoutedEventArgs e) //  Вывод иформации о продукте
        {
            foreach (var item in _warehouseService.Products.Where(item =>
                ((Image)sender).Tag.ToString() == item.Id.ToString()))
            {
                MessageBox.Show($"{item.Name}\n Цена Продажи: {item.CostSells}\n Цена Покупки: {item.CostBuy}");
                break;
            }
        }

        private async Task BuildingGridMouseDownAsync(object sender, MouseButtonEventArgs e) // Постройка здания
        {
            if (_tagChoiceBuilding >= 100)
            {
                var house = _houseService.HouseTypes.First(x => x.Id == _tagChoiceBuilding - 100);
                ((Border)sender).Tag = house;
                if (((Border)sender)?.Child is Image image)
                {
                    image.Source = new BitmapImage(new Uri(@"pack://application:,,,/" + house.HouseIconPath));
                    image.MouseLeftButtonDown -= BuildShowInfoClick;
                    image.MouseLeftButtonDown += HouseShowInfoClick;
                    image.Tag = await _houseService.ConstractionHouseAsync(house,
                        new Point(Grid.GetRow((Border)sender), Grid.GetColumn((Border)sender)));
                }

                _tagChoiceBuilding = 0;
            }
            else if (_productBuildService.ProductionBuildings.FirstOrDefault(x => x.Id == _tagChoiceBuilding) is
                ProductionBuildingDTO buildingDto)
            {
                if (_tagChoiceBuilding != 0 && (sender as Border)?.Tag == null)
                {
                    if (_coinUser >= buildingDto.CostCoin)
                    {
                        _coinUser -= buildingDto.CostCoin;
                        coinTextBlock.Text = _coinUser.ToString();
                        var build = _productBuildService.ProductionBuildings.First(x => x.Id == _tagChoiceBuilding);
                        ((Border)sender).Tag = build;
                        ((Image)(sender as Border)?.Child).Source =
                            new BitmapImage(new Uri(@"pack://application:,,,/" + build.IconPath));
                        build.Power = 100;
                        ((Image)(sender as Border)?.Child).Tag = await _productBuildService.
                            BuildingConstructionAsync(build, new Point(Grid.GetRow(sender as Border ?? throw new InvalidOperationException()),
                                    Grid.GetColumn((Border)sender)));
                        if (choiceElemImage.Tag == null) choiceElemImage.Source = null;

                        _tagChoiceBuilding = 0;
                    }
                    else
                    {
                        MessageBox.Show("Ваш капитал исчерпан!");
                    }
                }
            }

            else if ((sender as Border)?.Tag != null && choiceElemImage.Tag != null)
            {
                switch (choiceElemImage.Tag?.ToString())
                {
                    case "expander":
                        _tagChoiceBuilding = ((ProductionBuildingDTO)(sender as Border).Tag).Id;
                        ((Image)((Border)sender).Child).Source = null;
                        ((Border)sender).Tag = null;
                        break;
                    case "pipette":
                        _tagChoiceBuilding = ((ProductionBuildingDTO)(sender as Border).Tag).Id;
                        break;
                    case "wrecking-ball":
                        int id = 0;
                        if (int.TryParse((((Border)sender).Child as Image).Tag.ToString(), out id))
                        {
                            if (((Border)sender).Tag is ProductionBuildingDTO) await _productBuildService.BuildRemuve(id);

                            if (((Border)sender).Tag is HouseTypeDTO) await _productBuildService.BuildRemuve(id);

                            ((Image)((Border)sender).Child).Source = null;
                            ((Border)sender).Tag = null;
                        }

                        break;
                }
            }
        }

        private async Task CreateProduct(ProductDTO product, int progress) // Событе добавления продукта на склад
        {
            await Task.Run(() =>
            {
                if (Dispatcher == null || product == null) return;

                Dispatcher.Invoke(() => _warehouseService.AddProductsAsync(product, 1));
                Dispatcher.Invoke(InitialWarehouse); // Запуск в главном потоке
            });
        }

        private async Task PlusProgress(byte progress)
        {
            await Task.Run(() =>
            {
                Dispatcher?.Invoke(() => _progressBar.Value = progress);
            });
        }

        private void MoveImage_MouseDown(object sender, MouseButtonEventArgs e) // Событе клика по элементу Muve 
        {
            choiceElemImage.Tag = (sender as Image)?.Tag;
            choiceElemImage.Source = (sender as Image).Source;
        }

        private void MaingridMouseRightButtonDown(object sender, MouseButtonEventArgs e) // Событе сброса 
        {
            choiceElemImage.Tag = null;
            choiceElemImage.Source = null;
            _tagChoiceBuilding = 0;
            optionsBuildBorder.Visibility = Visibility.Hidden;
        }

        private async Task BuildingRental(int rental)
        {
            await Task.Run(() =>
            {
                _coinUser -= rental;
                Dispatcher?.Invoke(() => coinTextBlock.Text = _coinUser.ToString()); ;
            });
        }


        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            var saveWindow = new SaveWindow(_saveService, _coinUser);
            saveWindow.ShowDialog();
        }
    }
}