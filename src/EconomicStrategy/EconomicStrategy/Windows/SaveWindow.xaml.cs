﻿using System.Windows;
using System.Windows.Controls;
using BusinessLogic.Service;

namespace EconomicStrategy.Windows
{
    /// <summary>
    ///     Логика взаимодействия для SaveWindow.xaml
    /// </summary>
    public partial class SaveWindow : Window
    {
        private readonly double _cahs;
        private readonly SaveService _saveService;

        public SaveWindow(SaveService saveService, double cahs)
        {
            _cahs = cahs;
            _saveService = saveService;
            InitializeComponent();
        }

        private async void saveButton_Click(object sender, RoutedEventArgs e)
        {
            await _saveService.SaveAsync(saveListBox.Items.Count + 1, nameTextBlock.Text, _cahs);
            saveListBox.Items.Add(new TextBlock {Text = nameTextBlock.Text});
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var list = await _saveService.GetNameSaveAsync();
            foreach (var item in list) saveListBox.Items.Add(new TextBlock {Text = item});
        }

        private async void loadButton_Click(object sender, RoutedEventArgs e)
        {
            await _saveService.LoadAsync(saveListBox.SelectedIndex + 1);
            Close();
        }
    }
}