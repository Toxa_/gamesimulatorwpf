﻿using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.Repository;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;

        public UnitOfWork(DatabaseContext context, HouseTypeRepository houseTypeRepository,
            ProductionBuildingRepository productionBuildingRepository,
            ProductRepository productRepository,
            SaveCellRepository saveCellRepository, BuildingProductionModelRepository buildingProductionModelRepository,
            CellModelRepository cellModelRepository, HouseModelRepository houseModelRepository,
            WarehouseModelRepository warehouseModelRepository)
        {
            _context = context;
            HouseTypeRepository = houseTypeRepository;
            ProductionBuildingRepository = productionBuildingRepository;
            ProductRepository = productRepository;
            SaveCellRepository = saveCellRepository;
            BuildingProductionModelRepository = buildingProductionModelRepository;
            CellModelRepository = cellModelRepository;
            HouseModelRepository = houseModelRepository;
            WarehouseModelRepository = warehouseModelRepository;
        }

        public HouseTypeRepository HouseTypeRepository { get; }
        public ProductionBuildingRepository ProductionBuildingRepository { get; }
        public ProductRepository ProductRepository { get; }
        public SaveCellRepository SaveCellRepository { get; }
        public BuildingProductionModelRepository BuildingProductionModelRepository { get; }
        public CellModelRepository CellModelRepository { get; }
        public HouseModelRepository HouseModelRepository { get; }
        public WarehouseModelRepository WarehouseModelRepository { get; }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}