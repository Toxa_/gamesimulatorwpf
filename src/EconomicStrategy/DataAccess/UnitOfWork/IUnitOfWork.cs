﻿using System.Threading.Tasks;
using DataAccess.Repository;

namespace DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        HouseTypeRepository HouseTypeRepository { get; }
        ProductionBuildingRepository ProductionBuildingRepository { get; }
        ProductRepository ProductRepository { get; }
        SaveCellRepository SaveCellRepository { get; }
        BuildingProductionModelRepository BuildingProductionModelRepository { get; }
        CellModelRepository CellModelRepository { get; }
        HouseModelRepository HouseModelRepository { get; }
        WarehouseModelRepository WarehouseModelRepository { get; }
        Task SaveAsync();
    }
}