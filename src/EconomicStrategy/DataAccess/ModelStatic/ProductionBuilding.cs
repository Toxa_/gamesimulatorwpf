﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.ModelStatic
{
    public class ProductionBuilding
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
        public string IconPath { get; set; }
        public double CoolDawn { get; set; }
        public int Rental { get; set; }
        public int CostCoin { get; set; } // Стоимость строительства
        public IEnumerable<Product> BuildingResources { get; set; } // ресурсы для строительства
        public IEnumerable<ProductProductionBuilding> ProductProductionBuildings { get; set; } // потребляемые ресурсы
        public int FinalProductId { get; set; }
        public Product FinalProduct { get; set; }

        public byte Power { get; set; }
    }
}