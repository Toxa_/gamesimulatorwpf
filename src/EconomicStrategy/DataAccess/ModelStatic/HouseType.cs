﻿using System.Collections.Generic;

namespace DataAccess.ModelStatic
{
    public class HouseType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HouseIconPath { get; set; }
        public int Profit { get; set; }
        public int MaxCitizensInHouse { get; set; }
        public IEnumerable<Product> NeedProducts { get; set; }
        public string CitizenIconPath { get; set; }
    }
}