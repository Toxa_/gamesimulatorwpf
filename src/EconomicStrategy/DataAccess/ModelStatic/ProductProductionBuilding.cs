﻿namespace DataAccess.ModelStatic
{
    public class ProductProductionBuilding
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int ProductionBuildingId { get; set; }
        public ProductionBuilding ProductionBuilding { get; set; }
    }
}