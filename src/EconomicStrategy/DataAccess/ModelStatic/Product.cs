﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.ModelDynamic;

namespace DataAccess.ModelStatic
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
        public int CostBuy { get; set; }
        public int CostSells { get; set; }
        public string IconPath { get; set; }
        public ProductionBuilding FinalProduction { get; set; }
        public IEnumerable<CellModel> Cells { get; set; }
        public HouseType HouseType { get; set; } // тип дома потреюляемый этот продукт 
        public int? HouseTypeId { get; set; }

        public virtual IEnumerable<ProductProductionBuilding> ProductProductionBuildings { get; set; } =
            new List<ProductProductionBuilding>();
    }
}