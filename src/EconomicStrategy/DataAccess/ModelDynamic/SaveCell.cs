﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.ModelDynamic
{
    public class SaveCell
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
        public double Cash { get; set; }
        public IEnumerable<BuildingProductionModel> BuildingProductionModels { get; set; }
        public IEnumerable<HouseModel> HouseModels { get; set; }
        public IEnumerable<CellModel> CellModel { get; set; }
        public WarehouseModel WarehouseModel { get; set; }
    }
}