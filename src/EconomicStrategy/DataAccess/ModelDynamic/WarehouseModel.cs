﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.ModelDynamic
{
    public class WarehouseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public IEnumerable<CellModel> CellInWarehouses { get; set; }
        public int MaxSizeCell { get; set; } = 100;
        public int SaveCellId { get; set; }
        public SaveCell SaveCell { get; set; }
    }
}