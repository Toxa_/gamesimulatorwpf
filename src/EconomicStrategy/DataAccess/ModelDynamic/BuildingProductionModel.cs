﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.ModelStatic;

namespace DataAccess.ModelDynamic
{
    public class BuildingProductionModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public byte Power { get; set; }
        public IEnumerable<CellModel> Cells { get; set; }
        public int BuildingProductionId { get; set; }
        public ProductionBuilding BuildingProduction { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsWorked { get; set; }
        public byte Progress { get; set; }
        public int SaveCellId { get; set; }
        public SaveCell SaveCell { get; set; }
    }
}