﻿using DataAccess.ModelStatic;

namespace DataAccess.ModelDynamic
{
    public class CellModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int CountProduct { get; set; }
        public int MaxCountProduct { get; set; }
        public int? WarehouseModelId { get; set; }
        public WarehouseModel WarehouseModel { get; set; }
        public int? BuildingProductionModelId { get; set; }
        public BuildingProductionModel BuildingProductionModel { get; set; }
        public int? HouseModelId { get; set; }
        public HouseModel HouseModel { get; set; }
        public int? SaveCellId { get; set; }
        public SaveCell SaveCell { get; set; }
    }
}