﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.ModelStatic;

namespace DataAccess.ModelDynamic
{
    public class HouseModel
    {
        public int CountSatisFiedNeeds; // количество удовлетворяемых потребностей

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int Profit { get; set; }
        public int CurrentlyCountCitizens { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int HouseTypeId { get; set; }
        public HouseType HouseType { get; set; }
        public IEnumerable<CellModel> Cells { get; set; }
        public int SaveCellId { get; set; }
        public SaveCell SaveCell { get; set; }
    }
}