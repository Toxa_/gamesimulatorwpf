﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelStatic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class ProductRepository : BaseRepository<Product>
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<Product>> GetAllAsync()
        {
            return await Entities.Include(x => x.ProductProductionBuildings).Include(x => x.FinalProduction)
                .Include(x => x.HouseType).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<Product>> FindByConditionAsync(
            Expression<Func<Product, bool>> predicat)
        {
            return await Entities.Include(x => x.ProductProductionBuildings).Include(x => x.FinalProduction)
                .Include(x => x.HouseType).Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}