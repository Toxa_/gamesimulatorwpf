﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelDynamic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class WarehouseModelRepository : BaseRepository<WarehouseModel>
    {
        public WarehouseModelRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<WarehouseModel>> GetAllAsync()
        {
            return await Entities.Include(x => x.CellInWarehouses).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<WarehouseModel>> FindByConditionAsync(
            Expression<Func<WarehouseModel, bool>> predicat)
        {
            return await Entities.Include(x => x.CellInWarehouses).Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}