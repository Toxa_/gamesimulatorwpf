﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelStatic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class HouseTypeRepository : BaseRepository<HouseType> /*, IHouseTypeRepository*/
    {
        public HouseTypeRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<HouseType>> GetAllAsync()
        {
            return await Entities.Include(x => x.NeedProducts).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<HouseType>> FindByConditionAsync(
            Expression<Func<HouseType, bool>> predicat)
        {
            return await Entities.Include(x => x.NeedProducts).Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}