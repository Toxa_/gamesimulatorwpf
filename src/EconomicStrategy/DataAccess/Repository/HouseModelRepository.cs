﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelDynamic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class HouseModelRepository : BaseRepository<HouseModel>
    {
        public HouseModelRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public override async Task<IReadOnlyCollection<HouseModel>> GetAllAsync()
        {
            return await Entities.Include(x => x.Cells).Include(x => x.HouseType).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<HouseModel>> FindByConditionAsync(
            Expression<Func<HouseModel, bool>> predicat)
        {
            return await Entities.Include(x => x.Cells).Include(x => x.HouseType).Where(predicat).ToListAsync()
                .ConfigureAwait(false);
        }
    }
}