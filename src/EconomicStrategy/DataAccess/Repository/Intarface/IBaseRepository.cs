﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository.Intarface
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IReadOnlyCollection<T>> GetAllAsync();
        Task<IReadOnlyCollection<T>> FindByConditionAsync(Expression<Func<T, bool>> predicat);
        Task CreateAsync(T entity);
    }
}