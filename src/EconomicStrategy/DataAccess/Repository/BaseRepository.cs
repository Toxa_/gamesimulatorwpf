﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.Repository.Intarface;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private DbSet<T> _entities;

        protected BaseRepository(DatabaseContext context)
        {
            Context = context;
        }

        protected DatabaseContext Context { get; set; }

        protected DbSet<T> Entities
        {
            get
            {
                if (_entities == null) _entities = Context.Set<T>();
                return _entities;
            }
        }

        public virtual async Task<IReadOnlyCollection<T>> GetAllAsync()
        {
            return await Entities.ToListAsync().ConfigureAwait(false);
        }

        public virtual async Task<IReadOnlyCollection<T>> FindByConditionAsync(Expression<Func<T, bool>> predicat)
        {
            return await Entities.Where(predicat).ToListAsync().ConfigureAwait(false);
        }

        public async Task CreateAsync(T entity)
        {
            await Entities.AddAsync(entity).ConfigureAwait(false);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}