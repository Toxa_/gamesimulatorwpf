﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelStatic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class ProductionBuildingRepository : BaseRepository<ProductionBuilding> /*, IProductionBuildingRepository*/
    {
        public ProductionBuildingRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<ProductionBuilding>> GetAllAsync()
        {
            return await Entities.Include(x => x.ProductProductionBuildings).Include(x => x.FinalProduct).ToListAsync()
                .ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<ProductionBuilding>> FindByConditionAsync(
            Expression<Func<ProductionBuilding, bool>> predicat)
        {
            return await Entities.Include(x => x.ProductProductionBuildings).Include(x => x.FinalProduct)
                .Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}