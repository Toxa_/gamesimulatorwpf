﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelDynamic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class CellModelRepository : BaseRepository<CellModel>
    {
        public CellModelRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<CellModel>> GetAllAsync()
        {
            return await Entities.Include(x => x.BuildingProductionModel).Include(x => x.HouseModel)
                .Include(x => x.WarehouseModel).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<CellModel>> FindByConditionAsync(
            Expression<Func<CellModel, bool>> predicat)
        {
            return await Entities.Include(x => x.BuildingProductionModel).Include(x => x.HouseModel)
                .Include(x => x.WarehouseModel).Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}