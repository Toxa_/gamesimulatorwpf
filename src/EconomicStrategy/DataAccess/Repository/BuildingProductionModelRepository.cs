﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelDynamic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class BuildingProductionModelRepository : BaseRepository<BuildingProductionModel>
    {
        public BuildingProductionModelRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<BuildingProductionModel>> GetAllAsync()
        {
            return await Entities.Include(x => x.Cells).Include(x => x.BuildingProduction).ToListAsync()
                .ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<BuildingProductionModel>> FindByConditionAsync(
            Expression<Func<BuildingProductionModel, bool>> predicat)
        {
            return await Entities.Include(x => x.Cells).Include(x => x.BuildingProduction).Where(predicat).ToListAsync().ConfigureAwait(false);
        }
    }
}