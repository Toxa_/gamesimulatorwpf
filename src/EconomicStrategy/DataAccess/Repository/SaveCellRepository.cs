﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Context;
using DataAccess.ModelDynamic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class SaveCellRepository : BaseRepository<SaveCell>
    {
        public SaveCellRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<IReadOnlyCollection<SaveCell>> GetAllAsync()
        {
            return await Entities.Include(x => x.BuildingProductionModels).Include(x => x.CellModel)
                .Include(x => x.HouseModels).Include(x => x.WarehouseModel).ToListAsync().ConfigureAwait(false);
        }

        public override async Task<IReadOnlyCollection<SaveCell>> FindByConditionAsync(
            Expression<Func<SaveCell, bool>> predicat)
        {
            return await Entities.Where(predicat).Include(x => x.BuildingProductionModels).ThenInclude(x=>x.BuildingProduction).
                Include(x => x.CellModel).Include(x => x.HouseModels).ThenInclude(x=>x.HouseType).Include(x => x.WarehouseModel).ThenInclude(x=>x.CellInWarehouses)
                .ThenInclude(x=>x.BuildingProductionModel).Include(x => x.WarehouseModel).ThenInclude(x => x.CellInWarehouses).ThenInclude(x=>x.HouseModel)
                .Include(x => x.WarehouseModel).ThenInclude(x => x.CellInWarehouses).ThenInclude(x => x.Product)
                .Include(x => x.WarehouseModel).ThenInclude(x => x.CellInWarehouses).ThenInclude(x => x.SaveCell)
                .Include(x => x.WarehouseModel).ThenInclude(x => x.CellInWarehouses).ThenInclude(x => x.WarehouseModel)
                .ToListAsync().ConfigureAwait(false);
        }
    }
}