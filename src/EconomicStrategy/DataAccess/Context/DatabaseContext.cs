﻿using DataAccess.ModelDynamic;
using DataAccess.ModelStatic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Context
{
    public class DatabaseContext : DbContext /*BaseContext<DatabaseLocalContext>*/
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            // Database.EnsureCreated();
            ModelInitializer.Initialize(this);
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<HouseType> Houses { get; set; }
        public virtual DbSet<ProductionBuilding> ProductionBuildings { get; set; }
        public virtual DbSet<ProductProductionBuilding> ProductProductionBuildings { get; set; }


        public virtual DbSet<SaveCell> SaveCells { get; set; }
        public virtual DbSet<BuildingProductionModel> BuildingProductionModels { get; set; }
        public virtual DbSet<CellModel> CellModels { get; set; }
        public virtual DbSet<HouseModel> HouseModels { get; set; }
        public virtual DbSet<WarehouseModel> WarehouseModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //one to one ProductionBuilding --> Product (FinalProduct)
            modelBuilder.Entity<ProductionBuilding>().HasOne(x => x.FinalProduct).WithOne(x => x.FinalProduction)
                .HasForeignKey<ProductionBuilding>(x => x.FinalProductId);
            //ProductProductionBuilding - one to many (Product <--> ProductionBuilding)
            modelBuilder.Entity<ProductProductionBuilding>().HasKey(sc => new {sc.ProductId, sc.ProductionBuildingId});

            modelBuilder.Entity<ProductProductionBuilding>().HasOne(x => x.Product)
                .WithMany(y => y.ProductProductionBuildings).HasForeignKey(x => x.ProductId);

            modelBuilder.Entity<ProductProductionBuilding>().HasOne(x => x.ProductionBuilding)
                .WithMany(y => y.ProductProductionBuildings).HasForeignKey(x => x.ProductionBuildingId)
                .OnDelete(DeleteBehavior.NoAction);

            // one to many HouseType --> Product 
            modelBuilder.Entity<Product>().HasOne(x => x.HouseType).WithMany(x => x.NeedProducts)
                .HasForeignKey(x => x.HouseTypeId);
            // one to many SaveCell --> BuildingProductionModel
            modelBuilder.Entity<BuildingProductionModel>().HasOne(x => x.SaveCell)
                .WithMany(x => x.BuildingProductionModels).HasForeignKey(x => x.SaveCellId);
            // one to many SaveCell --> HouseModel 
            modelBuilder.Entity<HouseModel>().HasOne(x => x.SaveCell).WithMany(x => x.HouseModels)
                .HasForeignKey(x => x.SaveCellId);
            // one to money CellModel --> HouseModel
            modelBuilder.Entity<CellModel>().HasOne(x => x.HouseModel).WithMany(x => x.Cells)
                .HasForeignKey(x => x.HouseModelId).OnDelete(DeleteBehavior.NoAction);
            // one to money CellModel --> Cell
            modelBuilder.Entity<CellModel>().HasOne(x => x.Product).WithMany(x => x.Cells)
                .HasForeignKey(x => x.ProductId).OnDelete(DeleteBehavior.NoAction);
            // one to many SaveCell --> CellModel
            modelBuilder.Entity<SaveCell>().HasMany(x => x.CellModel).WithOne(x => x.SaveCell)
                .HasForeignKey(x => x.SaveCellId).OnDelete(DeleteBehavior.NoAction);
        }
    }
}