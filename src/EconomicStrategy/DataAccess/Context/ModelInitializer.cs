﻿using System.Linq;
using DataAccess.ModelStatic;

namespace DataAccess.Context
{
    public static class ModelInitializer
    {
        public static void Initialize(DatabaseContext context)
        {
            context.Database.EnsureCreated();
            if (context.Products.Any()) return;

            context.Houses.Add(new HouseType
            {
                Name = "Дом крестьянина", MaxCitizensInHouse = 10, Profit = 1,
                HouseIconPath = @"Image\Icon\House\wood-cabin.png"
            });
            context.SaveChanges();

            // Заполнение Product

            #region

            context.Products.Add(new Product
            {
                Id = 1,
                Name = "Зерно",
                IconPath = @"Image\Icon\Product\wheat.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 2,
                Name = "Мука",
                IconPath = @"Image\Icon\Product\grain.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 3,
                Name = "Хлеб",
                HouseTypeId = 1,
                IconPath = @"Image\Icon\Product\sliced-bread.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 4,
                Name = "Виноград",
                IconPath = @"Image\Icon\Product\grapes.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 5,
                Name = "Вино",
                IconPath = @"Image\Icon\Product\wine-bottle.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 6,
                Name = "Хмель",
                IconPath = @"Image\Icon\Product\hops.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 7,
                Name = "Пиво",
                IconPath = @"Image\Icon\Product\beer-stein.png",
                CostBuy = 50,
                CostSells = 10
            });

            context.Products.Add(new Product
            {
                Id = 8,
                Name = "Кофейные зерна",
                IconPath = @"Image\Icon\Product\coffee-beans.png",
                CostBuy = 50,
                CostSells = 10
            });

            context.Products.Add(new Product
            {
                Id = 9,
                Name = "Кофе",
                IconPath = @"Image\Icon\Product\coffee-cup.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 10,
                Name = "Молоко",
                IconPath = @"Image\Icon\Product\milk-carton.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 11,
                Name = "Сыр",
                IconPath = @"Image\Icon\Product\cheese-wedge.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 12,
                Name = "Мясо",
                IconPath = @"Image\Icon\Product\meat.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 13,
                Name = "Стейки",
                IconPath = @"Image\Icon\Product\steak.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 14,
                HouseTypeId = 1,
                Name = "Мёд",
                IconPath = @"Image\Icon\Product\dripping-honey.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 15,
                Name = "Диамант",
                IconPath = @"Image\Icon\Product\cut-diamond.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 16,
                Name = "Золото",
                IconPath = @"Image\Icon\Product\gold-bar.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 17,
                Name = "Украшение",
                IconPath = @"Image\Icon\Product\big-diamond-ring.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 18,
                Name = "Пряжа",
                IconPath = @"Image\Icon\Product\yarn.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 19,
                HouseTypeId = 1,
                Name = "Ткань",
                IconPath = @"Image\Icon\Product\rolled-cloth.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 20,
                Name = "Дерево",
                IconPath = @"Image\Icon\Resource for build\wood-pile.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 21,
                Name = "Бумага",
                IconPath = @"Image\Icon\Product\papers.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 22,
                Name = "Книги",
                IconPath = @"Image\Icon\Product\book-pile.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 23,
                Name = "Камень",
                IconPath = @"Image\Icon\Resource for build\stone-pile.png",
                CostBuy = 50,
                CostSells = 10
            });
            context.Products.Add(new Product
            {
                Id = 24,
                Name = "Железо",
                IconPath = @"Image\Icon\Resource for build\i-beam.png",
                CostBuy = 50,
                CostSells = 10
            });
            //context.Products.Add(new Product()
            //{
            //    Id = 25,
            //    Name = "Кирпичи",
            //    IconPath = @"Image\Icon\Resource for build\brick-pile.png",
            //    CostBuy = 50,
            //    CostSells = 10
            //});

            #endregion // Заполнение 

            context.SaveChanges();

            // Заполнение Production Building

            #region

            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 1, Name = "Пшеничное поле", IconPath = @"Image\Icon\Building\granary.png", CoolDawn = 2 * 60,
                CostCoin = 200, FinalProductId = 1, Power = 100, Rental = 50
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 2, Name = "Мельница", IconPath = @"Image\Icon\Building\grain.png", CoolDawn = 30, CostCoin = 400,
                FinalProductId = 2, Rental = 70
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 3, Name = "Хлебопекарня", IconPath = @"Image\Icon\Building\factory.png", CoolDawn = 60,
                CostCoin = 600, FinalProductId = 3,Rental = 100
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 4, Name = "Плантация винограда", IconPath = @"Image\Icon\Building\Field grape.png", CoolDawn = 120,
                CostCoin = 300, FinalProductId = 4, Rental = 60
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 5, Name = "Винодельня", IconPath = @"Image\Icon\Building\refinery wine.png", CoolDawn = 60,
                CostCoin = 700, FinalProductId = 5, Rental = 100
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 6, Name = "Поле Хмеля", IconPath = @"Image\Icon\Building\Fild Hop.png", CoolDawn = 120,
                CostCoin = 300, FinalProductId = 6, Rental = 60
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 7, Name = "Пивоварня", IconPath = @"Image\Icon\Building\refinery beer.png", CoolDawn = 60,
                CostCoin = 600, FinalProductId = 7, Rental = 100
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 8, Name = "Кофейная плантация", IconPath = @"Image\Icon\Building\Field coffee.png", CoolDawn = 120,
                CostCoin = 350, FinalProductId = 8, Rental = 60
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 9, Name = "Кофеварня", IconPath = @"Image\Icon\Building\refinery coffee.png", CoolDawn = 60,
                CostCoin = 700, FinalProductId = 9, Rental = 100
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 10, Name = "Загон коров", IconPath = @"Image\Icon\Building\ranch-Cow.png", CoolDawn = 120,
                CostCoin = 200, FinalProductId = 10, Rental = 60
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 11, Name = "Сыроварня", IconPath = @"Image\Icon\Building\fabrick Chees.png", CoolDawn = 60,
                CostCoin = 400, FinalProductId = 11 , Rental = 120
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 12, Name = "Загон свиней", IconPath = @"Image\Icon\Building\ranch-Pig.png", CoolDawn = 120,
                CostCoin = 200, FinalProductId = 12, Rental = 70
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 13, Name = "Лавка мясника", IconPath = @"Image\Icon\Building\Steak Production.png", CoolDawn = 60,
                CostCoin = 350, FinalProductId = 13,Rental = 130
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 14, Name = "Пасека", IconPath = @"Image\Icon\Building\beehive.png", CoolDawn = 180, CostCoin = 100,
                FinalProductId = 14, Rental = 30
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 15, Name = "Шахта по добычи рубинов", IconPath = @"Image\Icon\Building\dimond-mine.png",
                CoolDawn = 30, CostCoin = 1000, FinalProductId = 15, Rental = 150
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 16, Name = "Шахта по добычи золота", IconPath = @"Image\Icon\Building\gold-mine.png",
                CoolDawn = 30, CostCoin = 1000, FinalProductId = 16, Rental = 150
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 17, Name = "Ювелирная", IconPath = @"Image\Icon\Building\Jewellry production.png", CoolDawn = 120,
                CostCoin = 2000, FinalProductId = 17, Rental = 200
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 18, Name = "Загон овец", IconPath = @"Image\Icon\Building\ranch-sheep.png", CoolDawn = 120,
                CostCoin = 200, FinalProductId = 18, Rental = 60
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 19, Name = "Ткацкая мастерская", IconPath = @"Image\Icon\Building\Cloth Productrion.png",
                CoolDawn = 60, CostCoin = 200, FinalProductId = 19, Rental = 80
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 20, Name = "Лесопилка", IconPath = @"Image\Icon\Building\axe-in-log.png", CoolDawn = 30,
                CostCoin = 300, FinalProductId = 20, Rental = 30
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 21, Name = "Бумажная фабрика", IconPath = @"Image\Icon\Building\peper production.png",
                CoolDawn = 60, CostCoin = 200, FinalProductId = 21, Rental = 70
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 22, Name = "Типография", IconPath = @"Image\Icon\Building\Book production.png", CoolDawn = 120,
                CostCoin = 400, FinalProductId = 22, Rental = 100
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 23, Name = "Каменоломня", IconPath = @"Image\Icon\Building\stoune-mine.png", CoolDawn = 60,
                CostCoin = 200, FinalProductId = 23, Rental = 50
            });
            context.ProductionBuildings.Add(new ProductionBuilding
            {
                Id = 24, Name = "Литейная", IconPath = @"Image\Icon\Building\Factory beam.png", CoolDawn = 60,
                CostCoin = 600, FinalProductId = 24, Rental = 150
            });
            //context.ProductionBuildings.Add(new ProductionBuilding() { Id = 25, Name = "Фабрика по изготовлению кирпичей", IconPath = @"Image\Icon\Building\Factory brick.png", CoolDawn = 60, CostCoin = 200, FinalProductId = 22 });

            #endregion

            context.SaveChanges();

            //Настройка связей(many to many) между Product и ProductionBuilding

            #region

            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 1, ProductionBuildingId = 2});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 2, ProductionBuildingId = 3});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 4, ProductionBuildingId = 5});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 6, ProductionBuildingId = 7});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 8, ProductionBuildingId = 9});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 10, ProductionBuildingId = 11});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 12, ProductionBuildingId = 13});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 15, ProductionBuildingId = 17});
            // context.ProductProductionBuildings.Add(new Models.ProductProductionBuilding() { ProductId = 16, ProductionBuildingId = 17 });
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 18, ProductionBuildingId = 19});
            context.ProductProductionBuildings.Add(new ProductProductionBuilding
                {ProductId = 20, ProductionBuildingId = 21});

            #endregion

            context.SaveChanges();
        }
    }
}