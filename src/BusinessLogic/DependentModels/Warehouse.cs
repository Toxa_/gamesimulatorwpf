﻿using System.Collections.Generic;

namespace BusinessLogic.DependentModels
{
    public class Warehouse
    {
        public List<Cell> CellInWarehouses { get; set; } = new List<Cell>();
        public int MaxSizeCell { get; set; }
    }
}