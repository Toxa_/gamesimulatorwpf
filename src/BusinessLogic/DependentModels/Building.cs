﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Timers;
using BusinessLogic.Models_DTO;
using DataAccess.ModelStatic;

namespace BusinessLogic.DependentModels
{
    public class Building : ProductionBuildingDTO
    {
        private double _interval;

        private Timer _timerProduction;
        private Timer _timerRental;

        public async Task InitialModel(ProductionBuildingDTO building)
        {
            await Task.Run(() =>
            {
                Cells = new List<Cell>();
                foreach (var item in building.FirstProducts) Cells.Add(new Cell { Product = item });

                Id = building.Id;
                Name = building.Name;
                IconPath = building.IconPath;
                CostCoin = building.CostCoin;
                CoolDawn = building.CoolDawn;
                Rental = building.Rental;
                Power = building.Power;
                FinalProduct = building.FinalProduct;
                BuildingResources = new List<ProductDTO>();
                FirstProducts = building.FirstProducts;
            });
        }
        //public field
        #region
        public delegate Task CreateProductEvent(ProductDTO product, Building building);

        public delegate Task<bool> FirstProductEvent(ProductDTO product, Building building);

        public delegate Task ShowProgressEvent(byte progress);
        public delegate Task RentalsEvent(int rental);

        public bool IsWorked { get; set; }
        public byte Progress { get; set; }
        public List<Cell> Cells { get; set; }
        public Point Point { get; set; }

        public event CreateProductEvent CreateProduct;
        public event ShowProgressEvent PlusProgressEvent;
        public event FirstProductEvent FirstProductFill;
        public event RentalsEvent RentalEvent;
        #endregion

        // function
        #region
        public async Task Start()
        {
            if (FirstProducts.Count != 0)
            {
                foreach (var item in FirstProducts)
                    if (!(await FirstProductFill.Invoke(item, this)))
                        Power = 0;
                return;
            }

            if (Power != 0)
            {
                IsWorked = true;
                _interval = CoolDawn / (Convert.ToDouble(Power) / 100) / 100 * 1000;
                _timerProduction = new Timer(_interval)
                {
                    Enabled = true,
                    AutoReset = true
                };
                _timerProduction.Start();

                _timerProduction.Elapsed += PlusProgress;
            }

            _timerRental = new Timer(60000);
            _timerRental.Enabled = true;
            _timerRental.Elapsed += RentalInvoke;
            _timerRental.Start();
        }

        private async void RentalInvoke(object sender, ElapsedEventArgs e)
        {
            await RentalEvent?.Invoke(Rental);
        }

        public void Continue()
        {
            Power = 100;
            IsWorked = true;
            _interval = CoolDawn / (Convert.ToDouble(Power) / 100) / 100 * 1000;
            _timerProduction = new Timer(_interval) { AutoReset = true, Enabled = true };
            _timerProduction.Elapsed += PlusProgress;
            _timerProduction.Start();
        }

        public async Task Stop()
        {
            Progress = 0;
            Power = 0;
            _timerProduction.Stop();
            _timerProduction.Enabled = false;
            _timerProduction.AutoReset = false;
            IsWorked = false;
            if (FirstProducts.Count != 0)
            {
                foreach (var item in Cells) item.CountProduct -= 1;
                var count = 0;
                foreach (var item in FirstProducts)
                    if (await FirstProductFill.Invoke(item, this))
                        count++;

                if (count == FirstProducts.Count) Continue();
            }
            else
            {
                Continue();
            }
        }

        public async Task Remove()
        {
            await Task.Run(() =>
            {
                if (_timerProduction != null && _timerRental != null)
                {
                    Progress = 0;
                    Power = 0;
                    _timerProduction?.Stop();
                    _timerProduction.Enabled = false;
                    _timerProduction.AutoReset = false;
                    _timerProduction = null;
                    _timerRental?.Stop();
                    _timerRental.Enabled = false;
                    _timerRental.AutoReset = false;
                    _timerRental = null;
                    IsWorked = false;
                }
            });
        }

        private async void PlusProgress(object sender, ElapsedEventArgs e)
        {
            if (Progress >= 100)
            {
                if(CreateProduct != null) await CreateProduct?.Invoke(FinalProduct, this);
                await Stop();
            }
            else
            {
                Progress++;
                PlusProgressEvent?.Invoke(Progress);
            }
        }
        #endregion
    }
}