﻿using BusinessLogic.Models_DTO;

namespace BusinessLogic.DependentModels
{
    public class Cell
    {
        public ProductDTO Product { get; set; }
        public int CountProduct { get; set; }
        public int MaxCountProduct { get; set; }
    }
}