﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace BusinessLogic.DependentModels
{
    public class TimerConsumeProduct
    {
        public delegate void ElapsedTimerEvent(int productId);

        private readonly Timer _timer;

        public TimerConsumeProduct(Timer timer, int productId)
        {
            _timer = timer;
            _timer.Elapsed += TimerElepsed;
            ProductId = productId;
        }

        public int ProductId { get; set; }
        public event ElapsedTimerEvent ElapsedTimer;

        public void Start()
        {
            _timer.Enabled = true;
            _timer.AutoReset = true;
            _timer.Start();
        }

        private void TimerElepsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            _timer.Enabled = false;
            _timer.AutoReset = false;
            ElapsedTimer.Invoke(ProductId);
        }
    }
}
