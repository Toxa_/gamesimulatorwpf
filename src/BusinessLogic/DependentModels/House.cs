﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using BusinessLogic.Models_DTO;

namespace BusinessLogic.DependentModels
{
    public class House : HouseTypeDTO
    {
        // fields
        #region

        public delegate Task<bool> FillNeedProductEvent(ProductDTO product, House house);

        public delegate Task PlusProfiEvent(int profit);

        private List<TimerConsumeProduct> _consumeProductsTimers;
        private int _countSatisfiedNeeds; // количество удовлетворяемых потребностей
        private Timer _profitTimer;

        public Point Point { get; set; }
        public List<Cell> Cells { get; set; }
        public int CurrentlyCountCitizens { get; set; } = 10;

        public event PlusProfiEvent PlusProfit;
        public event FillNeedProductEvent FillNeedProduct;

        #endregion

        public void InitialModel(HouseTypeDTO house)
        {
            Id = house.Id;
            Name = house.Name;
            HouseIconPath = house.HouseIconPath;
            MaxCitizensInHouse = house.MaxCitizensInHouse;
            CitizenIconPath = house.CitizenIconPath;
            Profit = house.Profit;
            NeedProducts = house.NeedProducts;
            Cells = new List<Cell>();
            _consumeProductsTimers = new List<TimerConsumeProduct>();
            foreach (var item in NeedProducts) Cells.Add(new Cell { Product = item });
        }
        // function
        #region
        public async Task StartLiving()
        {
            foreach (var item in Cells)
            {
                if (!(await FillNeedProduct.Invoke(item.Product, this))) continue;

                _countSatisfiedNeeds++;
                var timer = new TimerConsumeProduct(new Timer(300000), item.Product.Id);
                timer.ElapsedTimer += EndTimerConsumeProduct;
                _consumeProductsTimers.Add(timer);
            }

            Profit = (_countSatisfiedNeeds + 1) * 10 * CurrentlyCountCitizens;

            _profitTimer = new Timer(60000) {Enabled = true};
            _profitTimer.Elapsed += EndProfitTimer;
        }

        public void Continue(ProductDTO product)
        {
            if (_countSatisfiedNeeds != 3) _countSatisfiedNeeds++;

            Profit = (_countSatisfiedNeeds + 1) * 10 * CurrentlyCountCitizens;
            var timer = new TimerConsumeProduct(new Timer(300000), product.Id);
            timer.ElapsedTimer += EndTimerConsumeProduct;
            _consumeProductsTimers.Add(timer);
        }

        private void EndProfitTimer(object sender, ElapsedEventArgs e)
        {
            PlusProfit.Invoke(Profit);
        }

        private async void EndTimerConsumeProduct(int productId)
        {
            Cells.FirstOrDefault(x => x.Product.Id == productId).CountProduct--;
            if (await FillNeedProduct.Invoke(Cells.FirstOrDefault(x => x.Product.Id == productId).Product, this))
            {
                _consumeProductsTimers.First(x => x.ProductId == productId).Start();
            }
            else
            {
                _countSatisfiedNeeds--;
                _consumeProductsTimers.Remove(_consumeProductsTimers.First(x => x.ProductId == productId));
                Profit = _countSatisfiedNeeds * 10 * CurrentlyCountCitizens;
            }
        }
        #endregion
    }

}