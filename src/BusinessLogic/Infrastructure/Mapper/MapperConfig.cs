﻿using AutoMapper;
using BusinessLogic.Models_DTO;
using DataAccess.ModelStatic;

namespace BusinessLogic.Infrastructure.Mapper
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Product, ProductDTO>().ForMember(x => x.FistsProductionBuildings,
                y => y.MapFrom(src => src.ProductProductionBuildings));
            CreateMap<ProductProductionBuilding, ProductDTO>();
            CreateMap<ProductProductionBuilding, ProductionBuildingDTO>();
            CreateMap<HouseType, HouseTypeDTO>();
            CreateMap<ProductionBuilding, ProductionBuildingDTO>().ForMember(x => x.FirstProducts,
                y => y.MapFrom(src => src.ProductProductionBuildings));
        }
    }
}