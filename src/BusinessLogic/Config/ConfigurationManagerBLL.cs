﻿using BusinessLogic.Service;
using DataAccess.Context;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BusinessLogic.Config
{
    public class ConfigurationManagerBll
    {
        public static void Configuration(IServiceCollection service, string connection)
        {
            service.AddDbContext<DatabaseContext>(option => option.UseSqlServer(connection));
            service.AddTransient(typeof(BuildingProductionModelRepository));
            service.AddTransient(typeof(ProductRepository));
            service.AddTransient(typeof(HouseTypeRepository));
            service.AddTransient(typeof(ProductionBuildingRepository));
            service.AddTransient(typeof(SaveCellRepository));
            service.AddTransient(typeof(CellModelRepository));
            service.AddTransient(typeof(HouseModelRepository));
            service.AddTransient(typeof(WarehouseModelRepository));
            service.AddTransient(typeof(UnitOfWork));
            service.AddSingleton(typeof(WarehouseService));
            service.AddTransient(typeof(DatabaseContext));
        }
    }
}