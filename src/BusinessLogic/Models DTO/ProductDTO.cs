﻿using System.Collections.Generic;
using DataAccess.ModelStatic;

namespace BusinessLogic.Models_DTO
{
    public class ProductDTO
    {
        public ProductDTO()
        {
        }

        public ProductDTO(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            CostBuy = product.CostBuy;
            CostSells = product.CostSells;
            IconPath = product.IconPath;
            //FinalProductionBuilding = product.FinalProduction;
            FistsProductionBuildings = new List<ProductionBuildingDTO>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CostBuy { get; set; }
        public int CostSells { get; set; }

        public string IconPath { get; set; }

        //public ProductionBuilding FinalProductionBuilding { get; set; }
        public List<ProductionBuildingDTO> FistsProductionBuildings { get; set; }
    }
}