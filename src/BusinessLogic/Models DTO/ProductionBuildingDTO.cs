﻿using System.Collections.Generic;
using DataAccess.ModelStatic;

namespace BusinessLogic.Models_DTO
{
    public class ProductionBuildingDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IconPath { get; set; }
        public double CoolDawn { get; set; }
        public int Rental { get; set; }
        public int CostCoin { get; set; } // Стоимость строительства
        public virtual List<ProductDTO> BuildingResources { get; set; } // ресурсы для строительства
        public virtual List<ProductDTO> FirstProducts { get; set; } // потребляемые ресурсы
        public virtual ProductDTO FinalProduct { get; set; }
        public byte Power { get; set; }
    }
}