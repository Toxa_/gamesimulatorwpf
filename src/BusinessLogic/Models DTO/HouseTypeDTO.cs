﻿using System.Collections.Generic;
using DataAccess.ModelStatic;

namespace BusinessLogic.Models_DTO
{
    public class HouseTypeDTO
    {
        public HouseTypeDTO()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string HouseIconPath { get; set; }
        public int Profit { get; set; }
        public int MaxCitizensInHouse { get; set; }
        public List<ProductDTO> NeedProducts { get; set; }
        public string CitizenIconPath { get; set; }
    }
}