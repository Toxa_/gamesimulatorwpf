﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.DependentModels;
using BusinessLogic.Models_DTO;
using DataAccess.UnitOfWork;

namespace BusinessLogic.Service
{
    public class HouseService
    {
        public delegate Task PlusProfiEvent(int profit);

        private readonly IMapper _mapper;

        private readonly UnitOfWork _unitOfWork;

        private readonly List<ProductDTO>
            _waitProducts; // Лист ожиданий, в него добавляются продукты которые требуют жилые дома

        private readonly WarehouseService _warehouseService;

        public HouseService(UnitOfWork unitOfWork, WarehouseService warehouseService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _warehouseService = warehouseService;
            _warehouseService.AddProduct += CheckHouseNeedProductAsync;
            _mapper = mapper;

            _waitProducts = new List<ProductDTO>();
            Houses = new List<House>();
            HouseTypes = new List<HouseTypeDTO>();
        }

        public List<House> Houses { get; set; }
        public List<HouseTypeDTO> HouseTypes { get; set; }
        public event PlusProfiEvent PlusProfitEvent;

        public async Task InitialComponents() // Стартовая инициализация
        {
            HouseTypes = _mapper.Map<List<HouseTypeDTO>>(await _unitOfWork.HouseTypeRepository.GetAllAsync());
        }

        public async Task CheckHouseNeedProductAsync(Cell cell) // Отрабатывает при добавлении любого продукта на склад и проверяет лист ожиданий
        {
            await Task.Run(() =>
            {
                if (!(_waitProducts.FirstOrDefault(x => x.Name == cell.Product.Name) is ProductDTO product)) return;
                {
                    var house = Houses.FirstOrDefault(x => x.Cells.FirstOrDefault(x => x.Product.Name == cell.Product.Name)?.Product.Name == product.Name);
                    if (house == null) return;
                    else
                    {
                        cell.CountProduct--;
                        house.Cells.First(x => x.Product.Name == product.Name).CountProduct++;
                        house.Continue(house.Cells.First(x => x.Product.Name == product.Name).Product);
                        _waitProducts.Remove(product);
                    }
                }
            }).ConfigureAwait(false);
        }

        public async Task<int> ConstractionHouseAsync(HouseTypeDTO houseType, Point point) // Постройка здания
        {
            var house = new House();
            house.InitialModel(houseType);

            house.Id = Houses.Count + 1;
            house.Point = point;
            house.FillNeedProduct += FillHouseProduct;
            house.PlusProfit += PlusProfit;
            Houses.Add(house);
            await house.StartLiving();

            return house.Id;
        }

        public async Task HouseRemuve(int idHouse)
        {
            await Task.Run(() => { Houses.Remove(Houses.First(x => x.Id == idHouse)); });
        }

        public async Task PlusProfit(int profit)
        {
            await PlusProfitEvent.Invoke(profit);
        }

        public async Task<bool> FillHouseProduct(ProductDTO products, House house) // Заполняет дом продуктами, если их нет на складе добавляет их в лист ожиданий
        {
            return await Task.Run(() =>
            {
                if (_warehouseService.Warehouse.CellInWarehouses.FirstOrDefault(x => x.Product.Name == products.Name && x.CountProduct != 0) is Cell cell)
                {
                    cell.CountProduct--;
                    house.Cells.First(x => x.Product.Name == products.Name).CountProduct++;

                    return true;
                }

                _waitProducts.Add(products);

                return false;
            });
        }
    }
}