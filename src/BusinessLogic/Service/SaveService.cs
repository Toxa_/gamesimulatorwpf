﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.DependentModels;
using BusinessLogic.Models_DTO;
using DataAccess.ModelDynamic;
using DataAccess.UnitOfWork;

namespace BusinessLogic.Service
{
    public class SaveService
    {
        public delegate void LoadSavesEvent();

        private readonly HouseService _houseService;
        private readonly IMapper _mapper;
        private readonly ProductionService _productionService;
        private readonly UnitOfWork _unitOfWork;
        private readonly WarehouseService _warehouseService;

        public SaveService(IMapper mapper, HouseService houseService, ProductionService productionService,
            WarehouseService warehouseService, UnitOfWork unitOfWork)
        {
            _houseService = houseService;
            _productionService = productionService;
            _warehouseService = warehouseService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public double Cash { get; set; }
        public event LoadSavesEvent LoadSaveEvent;

        public async Task LoadAsync(int saveId)
        {
            _houseService.Houses.Clear();
            _productionService.Buildings.Clear();

            if ((await _unitOfWork.SaveCellRepository.FindByConditionAsync(x => x.Id == saveId))
                .Count != 0)
            {
                var saves = (await _unitOfWork.SaveCellRepository.FindByConditionAsync(x => x.Id == saveId)).First();
                this.Cash = saves.Cash;

                //await _unitOfWork.BuildingProductionModelRepository.FindByConditionAsync(x => x.SaveCellId == saveId);
                //await _unitOfWork.CellModelRepository.FindByConditionAsync(x => x.SaveCellId == saveId);
                //await _unitOfWork.HouseModelRepository.FindByConditionAsync(x => x.SaveCellId == saveId);

                //await _unitOfWork.ProductionBuildingRepository.GetAllAsync();
                //await _unitOfWork.CellModelRepository.GetAllAsync();

                foreach (var item in saves.BuildingProductionModels)
                {
                    item.BuildingProduction.Power = 100;
                    var id = await _productionService
                        .BuildingConstructionAsync(_mapper.Map<ProductionBuildingDTO>(item.BuildingProduction),
                            new Point(item.X, item.Y));
                    var build = _productionService.Buildings.FirstOrDefault(x => x.Id == id);
                    if (build != null) build.Progress = item.Progress;
                }

                foreach (var item in saves.HouseModels)
                {
                    var id = await _houseService
                        .ConstractionHouseAsync(_mapper.Map<HouseTypeDTO>(item.HouseType), new Point(item.X, item.Y));
                    var build = _houseService.Houses.FirstOrDefault(x => x.Id == id);
                    if (build != null) build.CurrentlyCountCitizens = item.CurrentlyCountCitizens;
                }

                if (saves.WarehouseModel != null)
                {
                    var warehouse = new Warehouse {MaxSizeCell = saves.WarehouseModel.MaxSizeCell};

                    foreach (var item in _warehouseService.Warehouse.CellInWarehouses)
                        item.CountProduct = saves.WarehouseModel.CellInWarehouses
                            .FirstOrDefault(x => x.Product.Id == item.Product.Id).CountProduct;
                }

                LoadSaveEvent?.Invoke();
            }
        }

        public async Task SaveAsync(int saveId, string name, double cash)
        {
            var saves = await _unitOfWork.SaveCellRepository.GetAllAsync();

            var saveCell = new SaveCell {Id = saveId, Name = name, Cash = cash};
            await _unitOfWork.SaveCellRepository.CreateAsync(saveCell);

            foreach (var item in _productionService.Buildings)
            {
                var id =
                    (await _unitOfWork.BuildingProductionModelRepository.GetAllAsync()).Count() +
                    1;
                var buildModel = new BuildingProductionModel
                {
                    Id = id, IsWorked = item.IsWorked, X = item.Point.X, Y = item.Point.Y, Power = item.Power,
                    Progress = item.Progress
                };

                var y = await _unitOfWork.ProductionBuildingRepository.FindByConditionAsync(x => x.Name == item.Name);
                buildModel.BuildingProductionId = (await _unitOfWork.ProductionBuildingRepository
                    .FindByConditionAsync(x => x.Name == item.Name)).First().Id;
                buildModel.SaveCellId = saveId;
                await _unitOfWork.BuildingProductionModelRepository.CreateAsync(buildModel);

                id++;
            }


            foreach (var item in _houseService.Houses)
            {
                var id = (await _unitOfWork.CellModelRepository.GetAllAsync()).Count() + 1;
                var houseModel = new HouseModel
                {
                    CurrentlyCountCitizens = item.CurrentlyCountCitizens, Id = id, X = item.Point.X, Y = item.Point.Y,
                    Profit = item.Profit, SaveCellId = saveId
                };
                var listCell = new List<CellModel>();

                foreach (var cell in item.Cells)
                    (houseModel.Cells as List<CellModel>)?.Add(new CellModel
                    {
                        MaxCountProduct = cell.MaxCountProduct, ProductId = cell.Product.Id, HouseModelId = id,
                        CountProduct = cell.CountProduct
                    });
                houseModel.Cells = listCell;

                houseModel.HouseTypeId = (await _unitOfWork.HouseTypeRepository
                    .FindByConditionAsync(x => x.Name == item.Name)).First().Id;

                await _unitOfWork.HouseModelRepository.CreateAsync(houseModel);
            }


            var warehouseModel = new WarehouseModel
            {
                Id = saveId,
                SaveCellId = saveId,
                MaxSizeCell = _warehouseService.Warehouse.MaxSizeCell,
                CellInWarehouses = new List<CellModel>()
            };
            foreach (var item in _warehouseService.Warehouse.CellInWarehouses)
            {
                var cellModel = new CellModel
                {
                    MaxCountProduct = item.MaxCountProduct, ProductId = item.Product.Id, WarehouseModelId = saveId,
                    CountProduct = item.CountProduct
                };
                (warehouseModel.CellInWarehouses as List<CellModel>)?.Add(cellModel);
            }

            await _unitOfWork.WarehouseModelRepository.CreateAsync(warehouseModel);
        }

        public async Task<List<string>> GetNameSaveAsync()
        {
            var list = await _unitOfWork.SaveCellRepository.GetAllAsync().ConfigureAwait(false);
            return list.Select(item => item.Name).ToList();
        }
    }
}