﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.DependentModels;
using BusinessLogic.Models_DTO;
using DataAccess.UnitOfWork;

namespace BusinessLogic.Service
{
    public class WarehouseService
    {
        public delegate Task AddProductionEvent(Cell cell);

        private readonly IMapper _mapper;

        private readonly UnitOfWork _unitOfWork;

        public WarehouseService(UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            Products = new List<ProductDTO>();
        }

        public Warehouse Warehouse { get; set; } = new Warehouse();
        public List<ProductDTO> Products { get; set; }
        public event AddProductionEvent AddProduct;

        public async Task AddProductsAsync(ProductDTO product, int count) // Добавление Продуктов
        {
            await Task.Run(() =>
            {
                Warehouse.CellInWarehouses.FirstOrDefault(x => x.Product.Id == product.Id).CountProduct += count;
                var cell = Warehouse.CellInWarehouses.FirstOrDefault(x => x.Product.Id == product.Id);
                AddProduct.Invoke(cell);
            }).ConfigureAwait(false);
        }

        public async Task InitialWarehouseAsync() // Стартовая инициализация
        {
            if (await _unitOfWork.ProductRepository.GetAllAsync() != null)
                Products = _mapper.Map<List<ProductDTO>>(await _unitOfWork.ProductRepository.GetAllAsync()
                    .ConfigureAwait(false));

            foreach (var item in Products) Warehouse.CellInWarehouses.Add(new Cell { Product = item });
        }
    }
}