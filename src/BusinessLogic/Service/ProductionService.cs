﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.DependentModels;
using BusinessLogic.Models_DTO;
using DataAccess.UnitOfWork;

namespace BusinessLogic.Service
{
    public class ProductionService
    {
        public delegate Task BuildingCreateProductEvent(ProductDTO product, int progress);

        public delegate Task BuildingPlusProgressEvent(byte progress);
        public delegate Task BuildingRentalEvent(int rental);

        private readonly IMapper _mapper;

        private readonly List<ProductDTO> _productWaitingList;

        private readonly UnitOfWork _unitOfWork;

        private readonly WarehouseService
            _warehouseService; // Лист ожиданий, в него добавляются продукты которые требуются для производства

        public List<Building> Buildings;
        public List<ProductionBuildingDTO> ProductionBuildings;

        public ProductionService()
        {
        }

        public ProductionService(UnitOfWork unit, WarehouseService warehouseService, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unit;
            _warehouseService = warehouseService;
            _warehouseService.AddProduct += CheckWaitProductAsync;
            Buildings = new List<Building>();
            _productWaitingList = new List<ProductDTO>();
            ProductionBuildings = new List<ProductionBuildingDTO>();
        }

        public event BuildingCreateProductEvent CreateProductEvent;
        public event BuildingRentalEvent BuildingsRentalEvent;
        public event BuildingPlusProgressEvent PlusProgressEvent;

        public async Task BuildingRental(int rental)
        {
            await BuildingsRentalEvent?.Invoke(rental);
        }

        public async Task InitialCompontntsAsync() // Стартовая инициализация
        {
            ProductionBuildings =
                _mapper.Map<List<ProductionBuildingDTO>>(await _unitOfWork.ProductionBuildingRepository.GetAllAsync());
        }

        public Task CheckWaitProductAsync(Cell cellIn) // Отрабатывает при добавлении любого продукта на склад и проверяет лист ожиданий
        {
            return Task.Run(() =>
            {
                if (_productWaitingList?.FirstOrDefault(x => x.Name == cellIn.Product.Name) == null) return;

                foreach (var item in Buildings.Where(x => x.IsWorked == false && x.FirstProducts != null))
                    if (item.Cells.FirstOrDefault(x => x.Product.Name == cellIn.Product.Name) is Cell cell)
                    {
                        item.Cells.FirstOrDefault(x => x.Product.Name == cellIn.Product.Name).CountProduct += 1;
                        cellIn.CountProduct -= 1;
                        _productWaitingList.Remove(
                            _productWaitingList.FirstOrDefault(x => x.Name == cellIn.Product.Name));
                        item.Continue();
                        break;
                    }

            });
        }

        // Заполняет здание продуктами требуемых для производства, если их нет на складе добавляет их в лист ожиданий
        public async Task<bool> FillFirstProductBuilding(ProductDTO product, Building building)
        {
            if (_warehouseService.Warehouse.CellInWarehouses.FirstOrDefault(x => x.Product.Name == product.Name && x.CountProduct != 0) is Cell cell)
            {
                await Task.Run(() =>
                {
                    cell.CountProduct--;
                    building.Cells.First(x => x.Product.Id == product.Id).CountProduct++;
                });
                return true;
            }
            await Task.Run(() => { _productWaitingList.Add(product); });

            return false;
        }

        public async Task<int>
            BuildingConstructionAsync(ProductionBuildingDTO productionBuilding, Point point) // Постройка здания
        {
            var production = new Building();
            await production.InitialModel(productionBuilding);
            production.Id = Buildings.Count + 1;
            production.Point = point;

            Buildings.Add(production);
            production.FirstProductFill += FillFirstProductBuilding;
            production.CreateProduct += CreateProduct;
            production.RentalEvent += BuildingRental;

            await production.Start();

            return production.Id;
        }

        public async Task BuildRemuve(int idBuilding)
        {
            var build = Buildings.First(x => x.Id == idBuilding);
            build.PlusProgressEvent -= PlusProgress;
            build.FirstProductFill -= FillFirstProductBuilding;
            build.CreateProduct -= CreateProduct;
            build.RentalEvent -= BuildingRental;
            Buildings.Remove(Buildings.First(x => x.Id == idBuilding));
            await build.Remove().ConfigureAwait(false);
        }

        public void SubscribeEventPlusProgress(int id)
        {
            Buildings.FirstOrDefault(x => x.Id == id).PlusProgressEvent += PlusProgress;
        }

        public void UnsubscribeEventPlusProgress(int id)
        {
          if(Buildings.FirstOrDefault(x => x.Id == id) != null) Buildings.First(x => x.Id == id).PlusProgressEvent -= PlusProgress;
        }

        public async Task PlusProgress(byte progress)
        {
            await PlusProgressEvent?.Invoke(progress);
        }

        public async Task CreateProduct(ProductDTO product, Building building)
        {
            await CreateProductEvent.Invoke(product, building.Progress);
        }
    }
}